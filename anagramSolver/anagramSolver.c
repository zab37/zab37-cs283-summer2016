#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"getHash.h"

struct Node
{
	struct Node* next;
	char* word;
	int hash;
};

struct Node *newNode()
{
	struct Node *retNode = malloc(sizeof(struct Node));
	
	if(retNode == NULL)
		return NULL;
	
	retNode->next = malloc(sizeof(struct Node));
	retNode->word = malloc(128 * sizeof(char*));
	
	return retNode;	
}

struct Node* getDict()
{
	struct Node* retNode = newNode();
	FILE* dict = fopen("/words", "r");
	if(dict == NULL) {
	    return;
	}
	
	int hash;
	char wordList[128];
	struct Node* temp = retNode;
	
	while(fgets(wordList, sizeof(wordList), dict) != NULL) 
	{
		hash = getHashValue(wordList,sizeof(wordList));
		struct Node* node = newNode();
		strcpy(node->word, wordList);
		node->hash = hash;
		node->next = NULL;
		temp->next = node;
		temp = temp->next;
		
	}
	return retNode;
}

void printDict(struct Node* head)
{
	while(head->next != NULL)
	{
		printf("%s\n", head->word);
		head = head->next;
	}


}

struct Node* getSameHash(struct Node* head, int inputHash)
{

	struct Node* retNode = newNode();
	struct Node* temp = retNode;
	while(head->next != NULL)
	{
		if(inputHash == head->hash)
		{
			struct Node* node = newNode();
			node->hash = head->hash;
			strcpy(node->word, head->word);
			node->next = NULL;
			temp->next = node;
			temp = temp->next;

		}
		head = head->next;
	}	
	return retNode;
}

struct Node* getSameLength(struct Node* head, size_t length)
{
	struct Node* retNode = newNode();
	struct Node* temp = retNode;
	while(head->next != NULL)
	{
		if(length == strlen(head->word))
		{
			struct Node* node = newNode();
			node->hash = head->hash;
			strcpy(node->word, head->word);
			node->next = NULL;
			temp->next = node;
			temp = temp->next;
		}
		head = head->next;
	}	
	return retNode;

}

struct Node* getSameLetters(struct Node* head, size_t length, char* arg)
{
	int i;
	int k;
	int count;
	struct Node* retNode = newNode();
	struct Node* temp = retNode;
	
	while(head->next != NULL)
	{
	char* word = malloc((int)length);
	word = head->word;
	count = 0;
	for(i = 0;i < (int)length;i++)
	{
		for(k = 0;k< (int)length;k++)
		{
			if(arg[i] == word[k])
			{
				count += 1;
				k = (int)length;
			}	
		}
		
	}
	if(count == (int)length)
	{

		struct Node* node = newNode();
		node->hash = head->hash;
		strcpy(node->word, head->word);
		temp->next = node;
		temp = temp->next;
	}

	head = head->next;
	}
	return retNode;
}

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("\nPlease enter exactly 1 word as an argument.\n\n");
		return 1;
	}


	struct Node* dictHead = newNode();
	struct Node* sameHashHead = newNode();

	dictHead = getDict();

	int inputHash = getHashValue(argv[argc - 1], sizeof(argv[argc - 1]));

	sameHashHead = getSameHash(dictHead, inputHash);


	sameHashHead = getSameLength(sameHashHead, strlen(argv[argc - 1]) + 1);
	sameHashHead = getSameLetters(sameHashHead, strlen(argv[argc - 1]), argv[argc - 1]);
	printDict(sameHashHead);	
	free(sameHashHead);
	free(dictHead);

	return 0;
}
