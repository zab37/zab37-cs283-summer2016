import sys
import csv
import time
from multiprocessing.dummy import Pool as ThreadPool

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
    
NumThreads = int(sys.argv[2])
threadlist = []
f = open(sys.argv[1], 'rt')
fileRows =[]
reader = csv.reader(f)
next(reader)

for row in reader:
    fileRows.append(row)
    
f.close()
onTimeCount = [0] * 10
delayed = [0] * 10

def mapReduce(lst):
    for i in range(lst,len(fileRows)/NumThreads):
        if is_number(fileRows[i][0]):
                splitStr = fileRows[i][5].split()
                if is_number(splitStr[0]):
                    if int(splitStr[0]) < 6:
                        onTimeCount[int(fileRows[i][0]) % 10] += 1
                    else:
                        delayed[int(fileRows[i][0]) % 10]+= 1
                elif splitStr[0] == "On":
                    onTimeCount[int(fileRows[i][0]) % 10] += 1
                    
for i in range(0,NumThreads):
    threadlist.append(i * (len(fileRows)/NumThreads))
    
start = time.time()
pool = ThreadPool(NumThreads)
results = pool.map(mapReduce, threadlist)

for i in range(0,10):
    print "Line " , i , ":" , float(onTimeCount[i]) / (onTimeCount[i] + delayed[i])

print "Time:" , time.time() - start

