#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "csapp.h"

volatile unsigned int count = 0;
void* increment(void *vargp);
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int main() {

	clock_t begin, end;
	double runTime, avgTime;

	int j;
	int totalCount = 0;
	for (j = 0; j < 10; j++) 
	{
		count = 0;
		int i;

		begin = clock();
		for (i = 0; i < 100; i++)
	  	{
			pthread_t thread;
			pthread_create(&thread, NULL, increment, NULL);
			pthread_join(thread, NULL);
		}
	
		end = clock();
		runTime = (double)(end - begin) / CLOCKS_PER_SEC;
		avgTime += runTime;
		totalCount += count;

		printf("Count: %d\n", count);
		printf("Run time = %f\n", runTime);
	}

	avgTime = avgTime / 10;
	printf("\nAvg count = %d\n", totalCount/10);
	printf("\nAvg Time = %f\n", avgTime);
	return 0;
}          

void* increment(void *vargp) 
{
	int i;
	
	for (i = 0; i < 1000; i++) 
	{
		pthread_mutex_lock(&mutex);
		count++;
		pthread_mutex_unlock(&mutex);
	}

	return NULL;
}         

