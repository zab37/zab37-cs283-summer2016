#include <sys/stat.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>


int is_dir(const char* path)
{
	struct stat buf;

	if(stat(path, &buf) < 0)
	{
		return 0;
	}

	return S_ISDIR(buf.st_mode);
}


int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("Please enter two folder paths\n");
		return 1;
	}	
	if(argv[1] == argv[2])
	{
	printf("The two arguments may not be the same.\n");
	}
	if(!is_dir(argv[1]))
	{
		printf("The argument for directory 1 is not valid\n");
		return 1;
	}	
	if(!is_dir(argv[2]))
	{
		printf("The argument for directory 2 is not valid\n");
		return 1;
	}
	return 0;
}
