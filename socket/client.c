#include <stdio.h>
#include "csapp.h"
#include <stdlib.h>

void checkArg(int argc);

int main(int argc, char** argv)
{
	checkArg(argc);
	int clientfd, port;
	char buf[MAXLINE];
	char page[MAXLINE], host[MAXLINE];
	strcpy(host, argv[1]);
	strcpy(page, argv[2]);
	rio_t rio;

	port = atoi(argv[3]);
	clientfd = Open_clientfd(argv[1], port);
	sprintf(buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", page, host);

	Rio_readinitb(&rio, clientfd);
	Rio_writen(clientfd, buf, strlen(buf));	

	printf("\n");
	//Rio_readlineb(&rio, buf, MAXLINE);
	//Fputs(buf, stdout);
	int size;
	//Gets rest of file
	while(1)
	{
		size = Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);
		if((strcmp(buf,"\r\n") == 0))
		{
			break;
		}
	}
	Close(clientfd);	
	return 0;
}

void checkArg(int argc)
{
	if(argc != 4)
	{
		printf("Please enter exactly 2 arguments, the web server to connect to, and the page to grab.\n");
		exit(0);
	}
}
