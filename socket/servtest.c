#include <stdio.h>
#include "csapp.h"
#include <stdlib.h>



int main(int argc, char** argv)
{

	int clientfd, port;
	char buf[MAXLINE];
	char page[MAXLINE], host[MAXLINE];
	strcpy(host, argv[1]);
	strcpy(page, argv[2]);
	rio_t rio;

	port = 98521;
	printf("before\n");
	clientfd = Open_clientfd(argv[1], port);
	printf("after\n");
	sprintf(buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", page, host);

	Rio_readinitb(&rio, clientfd);


	Rio_writen(clientfd, buf, strlen(buf));	

	Rio_readlineb(&rio, buf, MAXLINE);
	Fputs(buf, stdout);
}

/*
	while(1)
	{
		Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);
		printf("\n");
		if(strlen(buf) <= 2)
		{
			break;
		}


	}
	while(1)
	{
		Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);
		if(buf == "SOMETHING HERE TO FIND END OF FILE")
		{
			printf("bruh");
			break;
		}

*/
