#include "csapp.h"
#include "util.h"

int main(int argc, char **argv) {
	int listenfd, connfd, port, clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp;
	int* threadfd;
	char buf[MAXLINE];
	pthread_t t1, t2;

	port = atoi(argv[1]); 
	listenfd = open_listenfd(port);
	char delim[2] = " ";
	char *token;
	token = strtok(buf,delim);

	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
							 sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		
		rio_t rio;
		Rio_readinitb(&rio, connfd);
		Rio_readlineb(&rio, buf, MAXLINE);
		Fputs(buf, stdout);		
		token = strtok(buf,delim);		
		token = strtok(NULL, delim);

		
		FILE * fp;
    	char * line = NULL;
	   size_t len = 0;
		ssize_t read;
		char fileLoc[2] = ".";
		strcat(fileLoc, token);


		fp = fopen(fileLoc, "r");
	
		while ((read = getline(&line, &len, fp)) != -1) {
//			printf("%s", line);
			Rio_writen(connfd,line,strlen(line));
		}
		Rio_writen(connfd,"\r\n\r\n", strlen("\r\n\r\n"));		
//		close(connfd);


		/*
		while(1)
		{		
			Rio_readlineb(&rio, buf, MAXLINE);
			Fputs(buf, stdout);
			close(connfd);
		}
		*/
	}
}
