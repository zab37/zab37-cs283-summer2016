#include "csapp.h"

void* receiver(void* arg){
	char buf[MAXLINE];
	rio_t rio;
	int* fd = (int*) arg;
	Rio_readinitb(&rio, *fd);
	while (1){
		Rio_readlineb(&rio, buf, MAXLINE);
		printf("Echo:");
		Fputs(buf, stdout);
	}
}

void* sender(void* arg){
	char buf[MAXLINE];
	int* fd = (int*) arg;
	printf("Enter message:"); fflush(stdout);
	while (Fgets(buf, MAXLINE, stdin) !=NULL){
		Rio_writen(*fd, buf, strlen(buf));
		printf("Enter message:"); fflush(stdout);
	}
}

