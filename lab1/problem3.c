#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct List
{
	int val;
	struct List* next;
};


int problem3(int *a, int size)
{

	int i, j, temp;
	for(i = 0; i < size;i++)
	{
		for(j = 0;j < size;j++)
		{
			if(*(a + i) < *(a + j))
			{
				temp = *(a + i);
				*(a + i) = *(a + j);
				*(a + j) = temp;
			}	
		}
	}

	for(i = 0; i < size;i++)
	{
		printf("%d\n", a[i]);
	}
	
	return 0;
}

int main()
{
	int* a;
	int array[4] = {1, 5, 3, 4};
	a=array;

	struct List *head = (struct List*)malloc(sizeof(struct List));
	struct List *temp = (struct List*)malloc(sizeof(struct List));
	head->val = 0;
	int i;
	temp = head;
	for(i =1;i <5;i++)
		{
		struct List *next = malloc(sizeof(*next));
		next->val = rand()%10;
		temp->next = next;
		temp = next;
		free(next);
		}
	problem3(a,5);
}
