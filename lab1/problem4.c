#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct List
{
	int val;
	struct List* next;
};


int problem4(struct List *list, int size)
{

	printf("List: ");
	struct List *ptr = list;
	int k;
	for(k =0;k<size;k++)
	{
   	printf("%d ", ptr->val);
		ptr=ptr->next;
	}

	putchar('\n');

	int i, j;

	struct List *temp,*head,*temp2;
	head = list;
	temp = head;
	for(i = 0; i < size;i++)
	{

		if (head->next != NULL && i > 0)
		{	
			head = head->next;
			temp = head;
		}
		for(j = 1;j < (size-i);j++)
		{
			if (head->next != NULL)
			{
				temp2 = temp;
				temp = temp->next;

				if(head->val > temp->val)
				{
					temp2->val = head->val;
					head->val = temp->val;
					temp->val = temp2->val;
														
				}
			}
		}
	}

	printf("List: ");
   ptr = list;
	
	for(k =0;k<size;k++)
	{
   	printf("%d ", ptr->val);
		ptr=ptr->next;
	}

	putchar('\n');

	
	free(ptr);
	free(temp);
	free(temp2);
	free(head);

	return 0;
}

int main()
{

	struct List *head = (struct List*)malloc(sizeof(struct List));
	struct List *temp = (struct List*)malloc(sizeof(struct List));
	head->val = 0;
	int i;
	temp = head;
	for(i =1;i <5;i++)
	{
		struct List *next = malloc(sizeof(*next));
		next->val = rand()%10;
		next->next = NULL;
		temp->next = next;
		temp = next;

	}
	problem4(head,5);
}
