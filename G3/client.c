#include <stdio.h>
#include "csapp.h"
#include <stdlib.h>
#include "rsa.h"

void sendLength(char* msg, int clientfd);
void encryptAndSend(char* msg, int clientfd, int e, int c, int d);

int main(int argc, char** argv)
{
	int clientfd, port;
	char buf[MAXLINE];
	char host[MAXLINE];

	strcpy(host, argv[1]);
	rio_t rio;

	port = atoi(argv[2]);
	clientfd = Open_clientfd(argv[1], port);

	char first[10];
	char second[10];
	long c,d,e,m;

	c = atoi(argv[4]);
	d = atoi(argv[5]);
	e = atoi(argv[3]);

	char input[MAXLINE];

while(1)
{
	fgets(input, MAXLINE, stdin);
	
	sendLength(input, clientfd);
	encryptAndSend(input,clientfd,e,c,d);
	Rio_readinitb(&rio, clientfd);
	Rio_readlineb(&rio, buf, strlen(input));
	fputs(buf,stdout);
	printf("\n");

//	Fputs(input,stdout);
//	printf("\n");
}

	Close(clientfd);	
	return 0;
}

void sendLength(char* msg, int clientfd)
{
	int* length = malloc(sizeof length);
//	printf("length = %d\n", (int)strlen(msg));
	*length = (int)strlen(msg);
	Rio_writen(clientfd, length, 1);
	
}

void encryptAndSend(char* msg, int clientfd, int e, int c, int d)
{
	int length = strlen(msg);
	int i;
	long* encr = malloc(sizeof encr);
	char* decr = malloc(sizeof decr);

	for(i = 0; i < length; i++)
	{
		*encr = encrypt(msg[i],e,c);
		*decr = decrypt(*encr,d,c);
//		printf("msg = %c\nEncrypted = %li\nDecrypted = %c\n\n", msg[i], *encr, *decr);
		Rio_writen(clientfd, encr, 2);
	}
}
