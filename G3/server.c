#include "csapp.h"
#include "util.h"
#include <stdio.h>
#include "rsa.h"
#include <pthread.h>

void* receive(void* args);
void sendBack(int connfd, char* msg);

int main(int argc, char **argv) {
	int listenfd, connfd, port, clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp;
	int* threadfd;
	char buf[MAXLINE];

	port = atoi(argv[1]); 
	listenfd = open_listenfd(port);
	char delim[2] = " ";
	char *token;
	long* buffer = malloc(sizeof buffer);
	token = strtok(buf,delim);

	int e = atoi(argv[2]);
	int d = atoi(argv[4]);
	int c = atoi(argv[3]);
	int threadcount = 0;
	int* args = malloc(3 * sizeof args);
	pthread_t* threads = malloc(sizeof(pthread_t)*threadcount);
	clientlen = sizeof(clientaddr);

while(1)
{
	connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
	threadcount += 1;
	threads = realloc(threads, sizeof(pthread_t)*threadcount);
	args[0] = connfd;
	args[1] = d;
	args[2] = c;
	pthread_create(&threads[threadcount],NULL,receive,(void* )args);
}
}

void* receive(void* args)
{
	int* getArgs = (int*)args;
	int connfd = getArgs[0];
	int d = getArgs[1];
	int c = getArgs[2];
	

	while(1)
	{
		long* buffer = malloc(sizeof buffer);
		rio_t rio;
		Rio_readinitb(&rio, connfd);
		Rio_readlineb(&rio, buffer,2);

		int i;
		long msgLen = *buffer;
		char* decr = malloc(msgLen * sizeof decr);

		for(i = 0;i<msgLen;i++)
		{
			Rio_readlineb(&rio, buffer, 3);
			decr[i] = decrypt(*buffer,d,c);
			printf("%c", decr[i]);
		}
		sendBack(connfd, decr);
	}
	Close(connfd);
}

void sendBack(int connfd, char* msg)
{
	Rio_writen(connfd,msg,strlen(msg));
		
}
