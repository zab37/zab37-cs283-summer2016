#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
long modulo(long a, long b, long c);
int gcd(int a, int b);
long coprime(int x);
long tcoprime(int m, int c);
long encrypt(int msg, int e, int c);
long decrypt(int cipher, int d, int c);
long mod_inverse(int base, int m);
long totient(int n);
long getNthPrime(int n);
int isprime(int n);
long* encryptString(char* msg, int len, long e, long c);
char* decryptString(long* cipher, int len, long d, long c);
long cracker(long e, long c);
