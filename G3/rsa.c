#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
long modulo(long a, long b, long c);
int gcd(int a, int b);
long coprime(int x);
long tcoprime(int m, int c);
long encrypt(int msg, int e, int c);
char decrypt(int cipher, int d, int c);
long mod_inverse(int base, int m);
long totient(int n);
long getNthPrime(int n);
int isprime(int n);
long* encryptString(char* msg, int len, long e, long c);
char* decryptString(long* cipher, int len, long d, long c);
long cracker(long e, long c);

/*
int main(int argc, char**argv)
{
	char first[10];
	char second[10];
	long a,b,c,d,e,m;

	printf("Enter the Nth prime\n");
	fgets(first,10,stdin);
	printf("Enter the Mth prime\n");
	fgets(second, 10, stdin);
	printf("%dth prime: %li\n%dth prime: %li\n",atoi(first), getNthPrime(atoi(first)),atoi(second), getNthPrime(atoi(second)));

	a = getNthPrime(atoi(first));
	b = getNthPrime(atoi(second));
	c = a*b;
	m = (a-1)*(b-1);
	e = tcoprime(m,c);
	d = mod_inverse(e,m);

	printf("a = %li\nb = %li\nc = %li\nd = %li\ne = %li\nm = %li\n",a,b,c,d,e,m);

	long cipher = encrypt(72,e,c);
	printf("H encrypted : %li\n", cipher);
	printf("H decrypted : %li\n", decrypt(cipher,d,c));//msg, d,c

	char msg[50] = "works";
	int len = strlen(msg);
	printf("\nlength %d\n", len);
	long* encr = encryptString(msg,len,e,c);
	char* decr = decryptString(encr,len,d,c);
	int i;

	for(i = 0; i < len; i++)
	{
		printf("%c %li\n",msg[i],encr[i]);
	}	

	printf("\n");

	for(i = 0; i < len; i++)
	{
		printf("%li %c\n",encr[i], decr[i]);
	}	
	
	long cracked = cracker(e,c);
	printf("cracked = %li\n", cracked);
	return 0;
}
*/

long cracker(long e, long c)
{
	long a = 1;
	long d = mod_inverse(e, totient(c));
	while(gcd(c,a) == 1)
	{
		a = a + 1;	
	}
	long b = c/a;
	return d;
}

char* decryptString(long* cipher, int len, long d, long c)
{
	char* result = malloc(len * sizeof result);
	int i;
	for(i = 0; i < len; i++)
	{
		result[i] = decrypt(cipher[i],d,c);
	}
	return result;
}

long* encryptString(char* msg,int len,long e, long c)
{
	long* result = malloc(len * sizeof result);
	int i;
	for(i = 0;i < len; i++)
	{
		result[i] = encrypt(msg[i],e,c);
	}
return result;
}

long modulo (long a, long b, long c)
{
    long result = 1;      // Initialize result
 
    a = a % c;  // Update x if it is more than or 
                // equal to p
 
    while (b > 0)
    {
        // If y is odd, multiply x with result
        if ((b % 2) != 0)
            result = (result*a) % c;
 
        // y must be even now
        b = b/2; // y = y/2
        a = (a*a) % c;  
    }
    return result;
}

int gcd(int a, int b)
{
	if(a == 0)
	{
		return b;
	}
	return gcd(b%a, a);
}

long totient(int n)
{
	long result = 1;
	long i;
	for(i = 2; i < n; i ++)
	{
		if(gcd(i,n) == 1)
		{
			result++;
		}
	}
	return result;
}

long tcoprime(int m, int c)
{

	while(1)
	{	
		long cp = coprime(m);
		if(gcd(cp, c) == 1)
		{
			return cp;
		}	
	}
	return 1;
}

long coprime(int x)
{
	srand(time(NULL));
	int r = rand() % totient(x);
	long result = 0;
	long i;
	for(i = 2; i < x; i ++)
	{
		if(gcd(i,x) == 1)
		{
			result++;
		}
		if(r == result)
		{
			return i;
		}
	}

}

long mod_inverse(int base, int m)
{
	long result;
	result = base % m;
	int i;
	for(i = 1; i < m;i++)
	{
		if((result * i) % m == 1)
		{
			result = i;
			i = m;
		}
	}	

	return result;
}

long encrypt(int msg, int e, int c)
{
	
	long result = modulo(msg,e,c);	

	return result;
}

char decrypt(int cipher, int d, int c)
{
	char result = modulo(cipher, d, c);	
	
	return result;
}

long getNthPrime(int n)
{
	long result = 0;
	long i = 1;
	while(result != n)	
	{
		i++;
		if(isprime(i) == 1 )
		{
			result++;
		}
	}
	return i;
}

int isprime(int n)
{
	if (n <= 1) return 0;
	if (n == 2) return 0;
//	if (n % 2 == 0) return 1;
	int i;
	for(i = 2; i < n; i++) //add last line back in, make this start at 3 and jump by 2 to check only odds?
	{
		if (n % i == 0)
			return 0;
	}
	return 1;
}
