#include <stdio.h>
#include "rsa.h"

int main(int argc, char** argv)
{

	long a,b,c,d,e,m;

	a = getNthPrime(atoi(argv[1]));
	b = getNthPrime(atoi(argv[2]));
	c = a*b;
	m = (a-1)*(b-1);
	e = tcoprime(m,c);
	d = mod_inverse(e,m);

	printf("a = %li\nb = %li\nc = %li\nd = %li\ne = %li\nm = %li\n",a,b,c,d,e,m);


	return 0;
}
